<div class="permission-tab-wrap">
    <h3><?php 
_e( 'Permission Management', 'wphr' );
?>
</h3>

    <form action="" class="permission-form wphr-form" method="post">

        <?php 
$is_manager = ( user_can( $employee->id, wphr_hr_get_manager_role() ) ? 'on' : 'off' );
wphr_html_form_input( array(
    'label' => __( 'HR Manager', 'wphr' ),
    'name'  => 'enable_manager',
    'type'  => 'checkbox',
    'tag'   => 'div',
    'value' => $is_manager,
    'help'  => __( 'This Employee is HR Manager', 'wphr' ),
) );
?>


        <?php 
do_action( 'wphr_hr_permission_management', $employee );
?>

        <input type="hidden" name="employee_id" value="<?php 
echo  $employee->id ;
?>
">
        <input type="hidden" name="wphr-action" id="wphr-employee-action" value="wphr-hr-employee-permission">
		
        <?php 
wp_nonce_field( 'wp-wphr-hr-employee-permission-nonce' );
?>
        <?php 
submit_button( __( 'Update Permission', 'wphr' ), 'primary' );
?>
    </form>

</div>
