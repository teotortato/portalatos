﻿=== WP-HR Manager: The Human Resources Plugin for WordPress ===
Plugin Name: WP-HR Manager
Plugin URI: http://www.wphrmanager.com/
Contributors: wphrmanager, freemius
Tags: HR, Human Resources, HRM, Attendance, Recruitment, Absence, Appraisal, Assessment, Leave, Clock In, Employee Self Service, ESS, HRIS, Human Resource, Personnel, People Management, WP-HR Manager, WPHR, job, jobs, job listing, vacancies, small business, SME, employee, leave management, holiday, check in, checkin, check out, checkout, GDPR
Requires at least: 4.4
Tested up to: 4.9.6
Stable tag: 0.1.8.1
License: GPLv2
Donate Link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GLKGN964GRZJW

Easily add a powerful HR / human resource management system and employee self service (ESS) portal to your website.

== Description ==

Now you can easily manage HR (Human Resource) records and processes from within your website with our exciting new plugin WP-HR Manager. You can quickly install an ESS (employee self-service) portal and HRM system, update staff records, track attendance and absence, message team members, approve leave and more.  

Ideal for small and medium sized businesses (SME) who want to create their own HR information system (HRIS) on WordPress.  

= WP-HR Manager enables you to: = 
* Install a powerful HRM system on your website to record and manage employee HR information via any browser 
* Take advantage of employee self service (ESS) features to reduce admin and improve accuracy (ideal for remote workers)
* Retain control of your data (and host your site/data in the location of your choice) helping with GDPR compliance
* Manage as many employees as you wish, with our free HR WordPress plugin: No incremental/per employee charges unlike cloud based HR systems
* Add features and plugins as you need them (even build your own!) with fully editable open source code
* Control access to data and capabilities with three inbuilt user levels (Admin, HR Manager and Employee)
* Keep it focussed - WP-HR Manager only adds HR features, keeping the plugin as lite as possible 
 
https://youtu.be/vygOR0o6Z-s

This plugin includes -

= WP-HR Manager Features =
* Company profile
* Branch listing - add local offices, outlets, factories etc.
* Employee profile - record and manage employee information
* Department listing - create departments and assign to employees
* Roles listing - create roles (eg 'Driver' or 'Branch Manager') and assign to employees
* Leave / holiday management - create and approve holiday, sickness, unpaid and other leave requests
* Set multiple leave / holiday policies to specify number of day's leave allowed, national (Bank) holiday dates, etc
* Monitor number of leave days taken and remaining for each employee
* Enable Employee Self Service (ESS) options so staff and update their own records
* Front end view for employees (discourages logged in employees from viewing WordPress backend screens)
* Employee Assessments and Appraisals - track performance and set goals
* WordPress admin dashboard customizing features
* Audit log - track changes to records
* 44 currencies supported
* Announcements feature - send to specific employees or all employees 
* Notification emails with custom templates and shortcode support

= Upgrade to WP-HR Manager Pro for additional features =
* Reports
* Import / export employee data
* Option to force employee to front end profile page on log in (restrict access to WP back end)
* Multiple Holiday Calendars (useful to create holiday sets for different countries)

= WP-HR Manager Extensions =
* __WP-HR Attendance__ (monitor check-in / check-out, add shifts)
* __WP-HR Recruitment__ (create and advertise job vacancies on your site and manage recruitment process)
* __WP-HR GDPR Pro__ (a set of useful tools to: Create your Privacy Policy; Record Consent to Manage Data from employees and job applicants; track employee GDPR training; log and manage Subject Access Requests; and more.) We also offer a stand alone lite version for free [here](https://wordpress.org/plugins/wp-hr-gdpr/) 
* More planned - watch this space!

These extensions can be added from with the WPHR Settings menu on your WordPress dashboard.

= Links =
* [Documentation](https://wphrmanager.com/documentation/)
* [Project Site](https://wphrmanager.com/)
* [Extensions](https://wphrmanager.com/extensions/)

= Translations (full or partial) =
* Bulgarian / Български
* Chinese (China) / 简体中文
* Danish / Dansk
* Dutch / Nederlands
* German / Deutsch
* Japanese / 日本語 
* Norwegian (Bokmål) / Norsk bokmål
* Polish / Polski
* Persian (Iranian)
* Spanish (Spain) / Español
* Swedish / Svenska

If you would like to help with translating this plugin, please go [here](https://translate.wordpress.org/projects/wp-plugins/wp-hr-manager/) 

= Press Coverage =

"WP-HR Manager is a new approach to HRM software - neither a clunky old desktop application, nor a cloud system with expensive per employee pricing. An exciting new tool for HR managers."

*Becki Clarke, Editor, www.HRreview.co.uk*

"A great tool for organisations of all sizes.  The free version does all the basics things you need - and then you can customize with add-ons to get the exact mix of functionality you want.  This one will just get better and better."

*Bill Banham, Publisher, www.hr-gazette.com*



== Installation ==
###Automatically Install From WordPress Dashboard

1. Login to your the admin panel
2. Navigate to Plugins -> Add New
3. Search **WP-HR Manager**
4. Click install and activate respectively.

###Manual Install From WordPress Dashboard

If your server is not connected to the Internet, then you can use this method:

1. Download the plugin by clicking on the blue 'Download' button above. A ZIP file will be downloaded.
2. Login to your site’s admin panel and navigate to Plugins -> Add New -> Upload.
3. Click 'Choose File', select the plugin file and click install.

###Install Using FTP

If you are unable to use any of the methods due to internet connectivity and file permission issues, then you can use this method:

1. Download the plugin by clicking on the blue 'Download' button above. A ZIP file will be downloaded.
2. Unzip the file.
3. Launch your favorite FTP client. Such as FileZilla, FireFTP, CyberDuck etc. If you are a more advanced user, then you can use SSH too.
4. Upload the folder to wp-content/plugins/
5. Log in to your WordPress dashboard.
6. Navigate to Plugins -> Installed
7. Activate the plugin.

== Screenshots ==

1. Easy to configure - built in setup wizard
2. Comprehensive employee records
3. Add information about employment status, compensation role, work location and reporting lines
4. Monitor and assign leave.  
5. Add notes and free-style employee information.
6. Create performance reviews, rate employee by key criteria, track manager comments and set goals.
7. Give employee enhanced capabilities by setting status to 'HR Manager' or (with Pro version) push default their access to front-end screens only on logon.
8. Create difference leave policies for different locations and job roles to take account of local practices.
9. Create employee announcements.  Distribute to all or selected employees.

== Frequently Asked Questions ==
= Do you have any limits on the number of admins, managers or employees?=
  No. 

= Does WP-HR Manager support WordPress multisite installation?=
  No.

= Can Employees Request Leave for Less than a Whole Day?=
  Yes - Leave can be booked in 15 minute increments

= Can I Have This or That Feature?=
  Yes - we love to receive ideas and suggestions [here](http://www.wphrmanager.com/feature-request/) and can also undertake bespoke projects [here](http://www.wphrmanager.com/services/)

= How does x work? Where is the documentation?=
  Comprehensive documentation can be found [here](https://wphrmanager.com/documentation/)


== Changelog ==

= v0.1.8.1 -> 24 July 2018 =
 * Fix - Leave totals not displaying correctly

= v0.1.8 -> 4 June 2018 =
 * Tweak - Improved labelling of leave management fields
 * Feature - Add option to book leave from front end view

= v0.1.7.5 -> 23 May 2018 =
 * Fix - Incorrect SDK added to last update
 * Feature - Additional translations included

= v0.1.7.4 -> 22 May 2018 =
 * Fix - Issue with leave available displaying incorrectly

= v0.1.7.3 -> 14 April 2018 =
 * Fix - Replace files dropped in previous upload

= v0.1.7.2 -> 12 April 2018 =
 * Fix - Incorrect Text Domain
 * Update - Freemius SDK

= v0.1.7.1 -> 22 February 2018 =
 * Fix - Resolve plugin clash 'cannot redeclare... etc' on installation 

= v0.1.7 -> 12 February 2018 =
 * Feature - Add multiple holiday calendars (eg for different countries) in Pro version
 * Feature - Add Danish language translation (thanks to Mark M)
 * Feature - Make leave applications more granular - you can now take less than a whole day
 * Fix - Updates Freemius SDK to V3 - should fix upgrading errors

= v0.1.6 -> 30 January 2018 =
 * Quick fix for Library error

= v0.1.5 -> 30 October 2017 =
 * Fixes plugin clash

= v0.1.4 -> 23 October 2017 =
 * Fixes missing reports

= v0.1.3 -> 03 October 2017 =
 * Fixes compatibility issues with mobiles

 * Improves responsive displays on front and back end
 * Added UK English, Polish and Lithuanian (part) translation files

= v0.1.2 -> 27 September 2017 =

 * Updated more code with WordPress slug

= v0.1.1 -> 24 September 2017 =

 * Updated Freemius code with WordPress slug

= v0.1 -> 24 September 2017 =

 * Beta Release



== Upgrade Notice ==

= v0.1.8.1 -> 24 July 2018 =
Fix - Leave total not displaying correctly

= v0.1.8 -> 4 June 2018 =
Tweak - Improved labelling of leave management fields
Feature - Add option to book leave from front end view

= v0.1.7.5 -> 23 May 2018 =
Fix - Incorrect SDK added to last update
Feature - Additional translations included 

= v0.1.7.4 -> 22 May 2018 =
Fixes issue with leave available displaying incorrectly

= v0.1.7.3 -> 14 April 2018 =
Replace files dropped in previous upload causing error message

= v0.1.7.2 -> 12 April 2018 =
Fixes incorrect Text Domain and updates Freemius SDK

= v0.1.7.1 -> 22 February 2018 =
Fixes plugin clash

= v0.1.7 -> 12 February 2018 =
Enhancements to leave management, licencing SDK and languages

= v0.1.6 -> 30 January 2018 =
Fixes Library Error

= v0.1.5 -> 31 October 2017 =
Fixes plugin clash

= v0.1.4 -> 23 October 2017 =
Fixes missing reports

= v0.1.3 -> 10 October 2017 =
Fixes compatibity issues with mobiles / theme and adds some translation files.

= v0.1.2 -> 27 September 2017 =

Minor fixes to compatibility with extensions

= v0.1.1 -> 24 September 2017 =

Updated Freemius code with WordPress slug

= v0.1 -> 24 September 2017 =
Initial Release






